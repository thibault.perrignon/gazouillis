//Q123==============================================================
var DataShare = require('nedb');

db = new DataShare()
db = {}

db.users = new DataShare()
db.messages = new DataShare()

db.users.loadDatabase()


//Q456==============================================================

m = {message:'test message',user:'test'}
db.messages.insert(m)


test = new Promise((resolve, reject) => {
    db.messages.find({user:'test'}, function (err, docs) {
        if (err) {
            reject(err)
        } else {
            resolve(docs)
        }
    })
})

'print test'
test.then(function (data) {
    console.log(data)
})

//Q7==============================================================

db.messages.find({}, {message:1,user:0,_id:1}, function (err, docs) {
    if (err) {
        reject(err)
    } else {
        resolve(docs)
    }
})

//Q8==============================================================
test = new Promise((resolve, reject) => {
    db.messages.find({user:'test'}, {message:1,user:0,_id:0}, function (err, docs) {
        if (err) {
            reject(err)
        } else {
            resolve(docs)
        }
    })
})

test.then(function (data) {
    console.log(data)
})

//Q9==============================================================
function getMessageId(userid, message) {
    return new Promise((resolve, reject) => {
        db.messages.find({user:userid, message:message}, {_id:1}, function (err, docs) {
            if (err) {
                reject(err)
            } else {
                resolve(docs[0]._id)
            }
        })
    })
}

//Q10==============================================================
getMessageId(0, 'test message').then(function (data) {
    console.log(data)
})

//Q11==============================================================
m = {author_id: 0, author_name: 'test', date: new Date(), message: 'test message'}

var inputDate = new Date(myDate.toISOString())
db.find({
    date: {$lte: inputDate}
})

var datedujourplus1heure = new Date() + 3600000
var datedujourmoins1heure = new Date() - 3600000
var datedujoursplus1jour = new Date() + 86400000

'print message less 1 hour'
test = new Promise((resolve, reject) => {
    db.messages.find({
        date: {$gte: datedujourmoins1heure}
    }, function (err, docs) {
        if (err) {
            reject(err)
        } else {
            resolve(docs)
        }
    })
})

//Q12==============================================================
'find message from userid 155 and 198'
test = new Promise((resolve, reject) => {
    db.messages.find({
        $or: [
            {author_id: 155},
            {author_id: 198}
        ]
    }, function (err, docs) {
        if (err) {
            reject(err)
        } else {
            resolve(docs)
        }
    })
})

//Q13==============================================================
'find message from friend of userid 256'
fs = {user_id:256, friends_id: [155, 198]}
db.friends.insert(fs)

test = new Promise((resolve, reject) => {
    db.friends.find({
        user_id: 256
    },{friends_id:1}, function (err, docs) {
        if (err) {
            reject(err)
        } else {
            db.messages.find({
                $or: docs
            },{message:1}, function (err, docs) {
                if (err) {
                    reject(err)
                } else {
                    resolve(docs)
                }
            })
        }
    })
})


