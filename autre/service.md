##Q5)
|Nom|createUser|
|---|---|
|URL|PUT /usr|
|Description|permet le creation d'un utiliseur|
|Parametre|login password firstname lastname|
|Format| ok/erreur |
|Exemple|{”id” : ”1” } ou {”status” : ”401”, ”message” : ”user inconnu” }, ”on ne peut pas se suivre soi-même”|
|Erreurs possible|il n’existe pas de user avec ce login, deja amis, user(userid) n’existe pas|
|Avancement|X|
|Classe/Fichier js|X|
|Infos add|X|

##Q8)
|Nom|login|
|---|---|
|URL|POST /api/user/login|
|Description|permet de creer une cle de connexion validee pendant|
|Parametre|login, password|
|Format|cle/erreur|
|Exemple|{”status” : ”403”, ”message” : ”login et/ou le mot de passe invalide(s)” } ou {”status” : ”200”,”message” : ”Login et mot de passe acceptes” }|
|Erreurs possible|user inconnu (401), password incorrect (401), multiconnexion, erreur interne|
|Avancement|X|
|Classe/Fichier js|X|032140 k
|Infos add|X|

##Q12)
|Nom|logout|
|---|---|
|URL|DELETE /api/user/userid/logout|
|Description|permet de fermer une connexion (session) d’un utilisateur|
|Parametre|X|
|Format|ok/erreur|
|Exemple|{”status” : ”401”, ”message” : ”Utilisateur inconnu” }ou ”status” : ”200”, ”message” : ”session fermee” }|
|Erreurs possible| aucun user ne correspond au userid en parametre|
|Avancement|X|
|Classe/Fichier js|X|
|Infos add|X|