const path = require('path');
const api = require('./api.js');
const apif = require('./apifriends.js');
const apim = require('./apimessages.js');
const cors = require('cors');

var DataStore = require('nedb');
db = {}
db.users = new DataStore({ filename: './src/entities/db/users.json',autoload: true});
db.messages = new DataStore({ filename: './src/entities/db/messages.json',autoload: true});
db.friends = new DataStore({ filename: './src/entities/db/friends.json',autoload: true});

//db.users.insert({login: 'admin', password: 'admin', lastname: 'admin', firstname: 'admin',user_id:'0'});

// Détermine le répertoire de base
const basedir = path.normalize(path.dirname(__dirname));
console.debug(`Base directory: ${basedir}`);

express = require('express');
const app = express()
app.use(cors({credentials:true,origin:"http://localhost:3000"}))
api_1 = require("./api.js");
api_2 = require("./apifriends.js");
api_3 = require("./apimessages.js");

const session = require("express-session");

app.use(session({
    secret: "technoweb rocks"
}));

app.use('/api', api.default(db));
app.use('/apifriends', apif.default(db));
app.use('/apimessages', apim.default(db));

// Démarre le serveur
app.on('close', () => {
});
exports.default = app;

