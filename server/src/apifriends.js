const express = require("express");
const Users = require("./entities/users.js");
const Friends = require("./entities/friends.js");

function init(db) {
    const router = express.Router();
    // On utilise JSON
    router.use(express.json());
    // simple logger for this router's requests
    // all requests to this router will first hit this middleware
    router.use((req, res, next) => {
        console.log('API: method %s, path %s', req.method, req.path);
        console.log('Body', req.body);
        next();
    });
    const users = new Users.default(db);
    const friends = new Friends.default(db);

    //createFriend
    router.route("/user/:user_id(\\w+)/friends")
        .post(async (req, res) => {
            try {
                const friend2 = await users.get(req.params.user_id);
                let friend = friend2.login
                const friend_id = req.params.user_id;
                const user = req.body.login;
                const user_id = await users.getId(user);

                if(!friend || !user) {
                    res.sendStatus(404);
                    return;
                }
                if(friend === user) {
                    res.status(403).send("You can't add yourself as a friend");
                    return;
                }
                else{
                    friends.areFriends(user_id, friend_id).then( f => {
                        if(f)
                            res.status(409).send("Vous êtes déjà amis");
                        else{
                            friends.createFriend(user,user_id, friend,friend_id)
                            .then( fid => res.status(201).send({id: fid}))
                            .catch( err => res.status(500).send(err));
                        }
                    })
                }
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //getListFriends
    router.route("/user/:user_id(\\w+)/friends")
        .get(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                const user_id = req.params.user_id;

                if (!user){
                    res.status(401).send("User not found");
                }
                else{
                    const friend = await friends.getList(user.login,user_id);
                    res.status(200).send(friend);
                }
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //getFriendRelationship (asymetric ex: user1 is friend with user2 but user2 is not friend with user1)
    router.route("/user/:user_id(\\w+)/friends/:friend_id(\\w+)")
        .get(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                const friend = await users.get(req.params.friend_id);
                const user_id = req.params.user_id;
                const friend_id = req.params.friend_id;

                if (!user || !friend){
                    res.sendStatus(404);
                }
                else{
                    friends.areFriends(user_id, friend_id).then( f => {
                        if(f)
                            res.status(200).json({
                                status: 200, 
                                message: "Vous êtes amis"
                            });
                        else
                            res.status(401).json({
                                status: 401,
                                message: "Vous n'êtes pas amis"
                            })
                    })
                }
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //deleteFriend
    router.route("/user/:user_id(\\w+)/friends/:friend_id(\\w+)")
        .delete(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                const friend = await users.get(req.params.friend_id);
                const user_id = req.params.user_id;
                const friend_id = req.params.friend_id;

                if (!user){
                    res.sendStatus(404);
                }
                else{
                    friends.areFriends(user_id, friend_id).then( f => {
                        if(f)
                        {
                            friends.deleteFriend(user_id, friend_id)
                            try{
                                res.status(200).send(`User ${user.login} unfollowed user ${friend.login}`);
                            } catch(e){
                                res.status(200).send(`User ${user.login} unfollowed an inexisting user`);
                            }
                        }
                        else
                            res.status(401).send("Vous n'êtes pas amis");
                    })
                }
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //getFriendInfo
    router.route("/user/:user_id(\\w+)/info")
        .get(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                if (!user){
                    res.sendStatus(404);
                }
                else{
                    const friend = await friends.getList(user.login,user_id);
                    res.status(200).json(friend);
                }
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    return router;
}
exports.default = init;

