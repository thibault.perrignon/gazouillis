class Friends {
  constructor(db) {
    this.db = db;
  }

  createFriend(user, user_id, friend, friend_id) {
    var f = { "login": user,"id":user_id, "loginFriend": friend ,"idFriend":friend_id };
    this.db.friends.insert(f);
    return this.getIdFriend(user_id, friend_id);
  }
  
  deleteFriend(user_id, friend_id) {
    this.db.friends.remove({ "id":user_id,"idFriend":friend_id });
  }

  getIdFriend(user_id, friend_id) {
    return new Promise((resolve, reject) => {
      this.db.friends.find({"id":user_id,"idFriend":friend_id }, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          resolve(docs[0]._id)
        }
      })
    })
  }

  getList(user,user_id) {
    return new Promise((resolve, reject) => {
      this.db.friends.find({ "login": user ,"id":user_id},{loginFriend:1,idFriend:1,_id:0}, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          resolve(docs);
        }
      })
    })
  }

  areFriends(user_id, friend_id) {
    return new Promise((resolve, reject) => {
      this.db.friends.find({ "id": user_id ,"idFriend":friend_id}, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          resolve(docs.length > 0);
        }
      })
    })
  }

}

exports.default = Friends;

