class Messages {
  constructor(db) {
    this.db = db;
  }

  createMessage(message, user, user_id) {
    const date = new Date().getTime();
    var m = { "message": message, "user": user, "user_id": user_id, "date": date, "lastEdit": date, "signal": 0 };
    this.db.messages.insert(m);
    return this.getIdMessage(user_id, date);
  }

  get(messageId) {
    return new Promise((resolve, reject) => {
      this.db.messages.find({ "_id": messageId }, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          resolve(docs[0]);
        }
      })
    })
  }

  async signal(messageId,userId) {
    let sv = await this.signalValue(messageId)
    this.db.messages.update({ "_id": messageId }, { $set: { "signal": sv+1 }, $push: { "whosignal": userId } });
  }

  signalValue(messageId) {
    return new Promise((resolve, reject) => {
      this.db.messages.findOne({ "_id": messageId }, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          resolve(docs.signal);
        }
      })
    })
  }

  setMessage(messageId, message, oldMessage) {
    const date = new Date().getTime();
    var m = { "message": message, "lastEdit": date };
    this.db.messages.update({ "_id": messageId }, { $set: m });
    var d = {}
    d[oldMessage] = message;
    return d
  }

  async fromUser(messageId, user_id) {
    return new Promise((resolve, reject) => {
      this.db.messages.find({ "_id": messageId, "user_id": user_id }, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          resolve(docs.length > 0);
        }
      })
    })
  }

  delete(messageId) {
    this.db.messages.remove({ "_id": messageId });
  }

  getIdMessage(user_id, date) {
    return new Promise((resolve, reject) => {
      this.db.messages.find({ "user_id": user_id, "date": date }, (err, docs) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(docs[0]._id);
        }
      });
    });
  }

  getList() {
    return new Promise((resolve, reject) => {
      this.db.messages.find({}, (err, docs) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(docs);
        }
      });
    });
  }

  getListFromFriend(friendId) {
    return new Promise((resolve, reject) => {
      this.db.messages.find({ "user_id": friendId }, (err, docs) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(docs);
        }
      });
    });
  }

  getListFromAllFriend(friendsId) {
    return new Promise((resolve, reject) => {
      this.db.messages.find({ "user_id": { $in: friendsId } }, (err, docs) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(docs);
        }
      });
    });
  }

  async getInfoMessageUser(user_id) {
    let m = await this.getListFromFriend(user_id)
    return { "count": m.length, "date": m.sort((a, b) => { return b.date - a.date })[0].date }
  }

  async getInfoAllMessage() {
    let m = await this.getList()
    return { "count": m.length, "date": m.sort((a, b) => { return b.date - a.date })[0].date }
  }
}

exports.default = Messages;

