class Users {
  constructor(db) {
    this.db = db;
  }

  create(login, password, lastname, firstname) {
    var u = { "login": login, "password": password, "lastname": lastname, "firstname": firstname }
    this.db.users.insert(u);
    return this.getId(login);
  }

  delete(userid) {
    this.db.users.remove({ "_id": userid });
  }

  list() {
    return new Promise((resolve, reject) => {
      this.db.users.find({}, function (err, docs) {
        if (err) {
          reject(err);
        } else {
          resolve(docs);
        }
      });
    });
  }
  count() {
    return new Promise((resolve, reject) => {
      this.db.users.count({}, (err, docs) => {
        if (err) reject(err);
        else resolve(docs);
      });
    });
  }

  async getId(login) {
    return new Promise((resolve, reject) => {
      this.db.users.find({ "login": login }, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          resolve(docs[0]._id)
        }
      })
    })
  }

  get(userid) {
    return new Promise((resolve, reject) => {
      this.db.users.find({ "_id": userid }, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          resolve(docs[0]);
        }
      })
    })
  }

  async exists(login) {
    return new Promise((resolve, reject) => {
      this.db.users.find({ "login": login }, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          resolve(docs.length > 0);
        }
      })
    })
  }

  checkpassword(login, password) {
    return new Promise((resolve, reject) => {
      this.db.users.find({ "login": login, "password": password }, function (err, docs) {
        if (err) {
          reject(err)
        } else {
          if(docs.length > 0){
            resolve(docs[0]._id);
          }
          else{
            resolve(null)
          }
        }
      })
    })
  }
}

exports.default = Users;

