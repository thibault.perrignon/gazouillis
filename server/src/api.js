const express = require("express");
const Users = require("./entities/users.js");

function init(db) {
    const router = express.Router();
    // On utilise JSON
    router.use(express.json());
    // simple logger for this router's requests
    // all requests to this router will first hit this middleware
    router.use((req, res, next) => {
        console.log('API: method %s, path %s', req.method, req.path);
        console.log('Body', req.body);
        console.log(req.session);
        next();
    });
    const users = new Users.default(db);

    router.post('/', (req, res) => {
        if (req.session.userid == undefined) {
            res.status(403).send('non connecté')
            return;
        }
        res.status(200).send(req.session)
        return;
    });

    //login
    router.post("/user/login", async (req, res) => {
        try {
            const { login, password } = req.body;
            // Erreur sur la requête HTTP
            if (!login || !password) {
                res.status(400).json({
                    status: 400,
                    "message": "Requête invalide : login et password nécessaires"
                });
                return;
            }
            if (! await users.exists(login)) {
                res.status(401).json({
                    status: 401,
                    message: "Utilisateur inconnu"
                });
                return;
            }
            let userid = await users.checkpassword(login, password);
            if (userid) {
                // Avec middleware express-session
                req.session.regenerate(function (err) {
                    if (err) {
                        res.status(500).json({
                            status: 500,
                            message: "Erreur interne"
                        });
                    }
                    else {
                        // C'est bon, nouvelle session créée
                        req.session.userid = userid;
                        res.status(200).json({
                            status: 200,
                            message: "Login et mot de passe accepté",
                            userid: userid
                        });
                    }
                })
                return;
            }
            // Faux login : destruction de la session et erreur
            req.session.destroy((err) => { });
            res.status(403).json({
                status: 403,
                message: "login et/ou le mot de passe invalide(s)"
            });
            return;
        }
        catch (e) {
            // Toute autre erreur
            res.status(500).json({
                status: 500,
                message: "erreur interne",
                details: (e || "Erreur inconnue").toString()
            });
        }
    });

    //getUserCount
    router.get("/user/count", async (req, res) => {
        try {
            const count = await users.count();
            res.status(200).json({
                status: 200,
                count: count
            });
            return;
        }
        catch (e) {
            // Toute autre erreur
            res.status(500).json({
                status: 500,
                message: "erreur interne",
                details: (e || "Erreur inconnue").toString()
            });
        }
    });

    //getListUsers
    router.get("/user/list", async (req, res) => {
        try {
            const list = await users.list();
            res.status(200).json({
                status: 200,
                list: list
            });
            return;
        }
        catch (e) {
            // Toute autre erreur
            res.status(500).json({
                status: 500,
                message: "erreur interne",
                details: (e || "Erreur inconnue").toString()
            });
        }
    });

    //getUser
    router
        .route("/user/:user_id(\\w+)")
        .get(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                if (!user)
                    res.sendStatus(404);
                else
                    res.send(user);
            }
            catch (e) {
                res.status(500).send(e);
            }
        })//deleteUser
        .delete((req, res, next) => {
            try {
                res.send(`delete user ${req.params.user_id}`)
                users.delete(req.params.user_id);
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //logout
    router.route("/user/:user_id(\\w+)/logout")
        .delete((req, res) => {
            try {
                req.session.destroy((err) => {
                    if (err) {
                        res.status(500).json({
                            status: 500,
                            message: "Erreur interne"
                        });
                    }
                    else {
                        res.status(200).json({
                            status: 200,
                            message: "Logout réussi"
                        });
                    }
                });
            }
            catch (e) {
                res.status(500).send(e);
            }
        });


    //createUser
    router.put("/user", (req, res) => {
        try {
            const { login, password, lastname, firstname } = req.body;
            if (!login || !password || !lastname || !firstname) {
                res.status(400).send("Missing fields");
            } else {
                users.exists(login).then(exists => {
                    if (exists) {
                        res.status(409).send("User already exists");
                    } else {
                        users.create(login, password, lastname, firstname)
                            .then((user_id) => res.status(201).send({ id: user_id }))
                            .catch((err) => res.status(500).send(err));
                    }
                });
            }
        }
        catch (e) {
            res.status(500).send(e);
        }
    });

    return router;
}
exports.default = init;

