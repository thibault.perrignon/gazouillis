const express = require("express");
const Users = require("./entities/users.js");
const Friends = require("./entities/friends.js");
const Messages = require("./entities/messages.js");

function init(db) {
    const router = express.Router();
    // On utilise JSON
    router.use(express.json());
    // simple logger for this router's requests
    // all requests to this router will first hit this middleware
    router.use((req, res, next) => {
        console.log('API: method %s, path %s', req.method, req.path);
        console.log('Body', req.body);
        next();
    });

    const users = new Users.default(db);
    const friends = new Friends.default(db);
    const messages = new Messages.default(db);

    //createMessage
    router.route("/user/:user_id(\\w+)/messages")
        .post(async (req, res) => {
            try {
                const user_id = req.params.user_id;
                const user = await users.get(user_id);
                const message = req.body.message;

                if (!user) {
                    res.status(404).send("User not found");
                    return;
                }
                if (!message) {
                    res.status(400).send("Message is empty");
                    return;
                }
                else {
                    const result = await messages.createMessage(message,user.login,user_id);
                    res.status(201).send(result);
                }

            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //setMessage
    router.route("/user/:user_id(\\w+)/messages")
        .put(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                const messageId = req.body.message_id;
                const message = req.body.message;

                const oldMessage = await messages.get(messageId);

                if (!user) {
                    res.status(404).send("User not found");
                    return
                }
                if (!oldMessage) {
                    res.status(404).send("Message not found");
                    return
                }
                if (!message.trim()) {
                    res.status(400).send("Message is empty");
                    return;
                }
                if (message === oldMessage.message) {
                    res.status(400).send("Message is the same");
                    return;
                }
                else {
                    const result = await messages.setMessage(messageId,message,oldMessage.message);
                    res.status(201).send(result);
                }

            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //deleteMessage
    router.route("/user/:user_id(\\w+)/messages/:message_id(\\w+)")
        .delete(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                const messageId = req.params.message_id;
                const user_id = req.params.user_id;

                if (!user) {
                    res.status(404).send("User not found");
                    return
                }
                if (await messages.fromUser(messageId,user_id)) {
                    messages.delete(messageId);
                    res.status(200).send("Message deleted");
                }
                else {
                    res.status(404).send("Message not from user");
                }
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //getListMessages
    router.route("/messages")
        .get(async (req, res) => {
            try {

                const result = await messages.getList();
                res.status(201).send(result);

            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //signalMessage
    router.route("/messages/:message_id(\\w+)/signal")
        .post(async (req, res) => {
            try {
                const messageId = req.params.message_id;
                const userId = req.body.user_id;

                messages.signal(messageId,userId);

                res.status(200).send("Message signaled");
            }
            catch (e) {
                res.status(500).send(e);
            }
        });



    //getListMessageFromAllFriends
    router.route("/user/:user_id(\\w+)/messages/friends")
        .get(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                const user_id = req.params.user_id;

                if (!user) {
                    res.status(404).send("User not found");
                    return
                }

                const friendOfUser = await friends.getList(user.login,user_id);


                var fou = [];
                for(let i = 0; i < friendOfUser.length; i++) {
                    fou.push(friendOfUser[i].idFriend);
                }
                fou.push(user_id);

                const result = await messages.getListFromAllFriend(fou);
                res.status(201).send(result);
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //getListMessageFromFriend
    router.route("/user/:user_id(\\w+)/messages/:friend_id(\\w+)")
        .get(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                const friend = await users.get(req.params.friend_id);
                const user_id = req.params.user_id;
                const friend_id = req.params.friend_id;

                if (!user) {
                    res.status(404).send("User not found");
                    return
                }
                if (!friend) {
                    res.status(404).send("Friend not found");
                    return
                }

                friends.areFriends(user_id,friend_id).then(async result => {
                    if (result) {
                        const result = await messages.getListFromFriend(friend_id);
                        res.status(201).send(result);
                    }
                    else {
                        res.status(400).send("You are not friends");
                    }
                });
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //getInfoMessageUser
    router.route("/user/:user_id(\\w+)/infos")
        .get(async (req, res) => {
            try {
                const user = await users.get(req.params.user_id);
                const user_id = req.params.user_id;

                if (!user) {
                    res.status(404).send("User not found");
                    return
                }

                const result = await messages.getInfoMessageUser(user_id);
                
                res.status(201).send(result);
            }
            catch (e) {
                res.status(500).send(e);
            }
        });

    //getInfoAllMessage
    router.route("/infos")
        .get(async (req, res) => {
            try {
                const result = await messages.getInfoAllMessage();
                res.status(201).send(result);
            }
            catch (e) {
                res.status(500).send(e);
            }
        });


    return router;
}
exports.default = init;

