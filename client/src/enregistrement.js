import './style.css';
import React, { Component } from 'react'

class Enregistrement extends Component {
    constructor(props) {
        super(props)
        this.state = { cor: false, loginUse: false }
    }
    cancel() {
        this.props.cancel()
    }
    enregistrement() {
        var prenom = document.getElementById("prenom").value
        var nom = document.getElementById("nom").value
        var login = document.getElementById("login").value
        var pw = document.getElementById("pw").value
        var confirmpw = document.getElementById("cpw").value

        this.setState({ cor: false ,loginUse: false })

        if (pw === confirmpw) {
            this.props.api.put('/api/user', { "login": login, "password": pw, "lastname": nom, "firstname": prenom })
                .then(res => {
                    console.log("Enregistrement réussi")
                    console.log(res.data)  //id de l'utilisateur
                    this.props.connect()
                })
                .catch(err => {
                    if (err.response.status === 400) {
                        console.log("Missing fields")
                    }
                    if (err.response.status === 409) {
                        console.log("Login already used")
                        this.setState({ loginUse: true })
                    }
                })
        } else {
            this.setState({ cor: true })
        }
    }
    render() {
        return (
            <div>
                <p className='titre'>Enregistrement</p>
                <div className='center'>
                    <form className='boitecoreg' onSubmit={(e) => e.preventDefault()}>
                        <div className='just'>
                            <div className='prenom'>
                                <p className='space'>Prenom</p>
                                <input type="text" id="prenom" name="prenom" required maxLength="32" size="10" className='space' />
                            </div>
                            <div className='prenom'>
                                <p className='space'>Nom</p>
                                <input type="text" id="nom" name="nom" required maxLength="32" size="10" className='space' />
                            </div>
                        </div>
                        <div className='just'>
                            Login
                            <input type="text" id="login" name="name" required maxLength="32" size="10" className='space' />
                        </div>
                        <div className='warn2'>
                            {this.state.loginUse ? 'Ce login est déjà utilisé' : ''}
                        </div>
                        <div className='just'>
                            Mot de passe
                            <input type="password" id="pw" name="pass" required maxLength="32" size="10" className='space' />
                        </div>
                        <div className='just'>
                            Confirmer
                            <input type="password" id="cpw" name="repass" required maxLength="32" size="10" className='space' />
                        </div>
                        <div className='warn2'>
                            {this.state.cor ? 'Les mots de passe ne correspondent pas' : ''}
                        </div>
                        <div className='just'>
                            <button onClick={() => this.enregistrement()}>Enregistrer</button>
                            <button onClick={() => this.cancel()}>Annuler</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Enregistrement;