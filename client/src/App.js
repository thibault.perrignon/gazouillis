
import React, { Component } from 'react'
import Connexion from './connexion.js';
import Enregistrement from './enregistrement.js';
import Main from './main.js';
import Profil from './profil.js';
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);

    const api = axios.create({withCredentials:true, baseURL: "http://localhost:4000", timeout: 1000});

    this.signin = this.signin.bind(this);
    this.login = this.login.bind(this);
    this.deco = this.deco.bind(this);
    this.connect = this.connect.bind(this);
    this.profil = this.profil.bind(this);
    this.accueil = this.accueil.bind(this);

    this.state = { page: '', connecte: false, api: api, userid: null, oid: null }
  }
  componentDidMount() {
    this.state.api.post('/api',{})
      .then(res => {
        this.connect(res.data.userid)
        this.setState({page: ''})
        this.setState({page: 'main'})
      })
      .catch(err => console.log('non connecté'))
    this.setState({page: 'main'})
    
  }
  signin() {
    this.setState({ page: 'enregistrement' })
  }
  login() {
    this.setState({ page: 'connexion' })
  }
  deco() {
    this.setState({ page: 'main', connecte: false , userid: null})
  }
  connect(id = null) {
    this.setState({ page: 'main', connecte: true })
    if (id) {
      this.setState({ userid: id })
    }
  }
  accueil() {
    this.setState({ page: 'main' })
  }
  profil(oid) {
    if (oid!==this.state.oid) {
      this.setState({ oid: oid })
    }
    this.setState({ page: 'profil'})
  }
  render() {
    switch (this.state.page) {
      case 'main':
        return <Main login={this.login} signin={this.signin} logout={this.deco} profil={this.profil} connecte={this.state.connecte} api={this.state.api} userid={this.state.userid}/>
      case 'connexion':
        return <Connexion connect={this.connect} cancel={this.deco} api={this.state.api} />
      case 'enregistrement':
        return <Enregistrement connect={this.login} cancel={this.deco} api={this.state.api} />
      case 'profil':
        return <Profil accueil={this.accueil} logout={this.deco} login={this.login} signin={this.signin} userid={this.state.userid} oid={this.state.oid} connecte={this.state.connecte} api={this.state.api} profil={this.profil}/>
      default:
        return <div>Erreur</div>
    }
  }
}

export default App;