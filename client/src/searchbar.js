import './style.css';
import React, { Component } from 'react'

class Searchbar extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    search() {
        var contact = document.getElementById('contact')
        var search = document.getElementById('search')
        this.props.search(search.value, contact.checked)
    }
    render() {
        return (
            <form className='searchbar' onSubmit={(e) => e.preventDefault()}>
                <input type="text" id="search" name="search" size="60" />
                <button onClick={() => this.search()}>Rechercher</button>
                <div>
                    {this.props.connecte?<input type="checkbox" id="contact" name="contact" />:<input type="checkbox" id="contact" disabled/>}
                    <label htmlFor="contact">Contact uniquement</label>
                </div>
            </form>
        );
    }
}

export default Searchbar;