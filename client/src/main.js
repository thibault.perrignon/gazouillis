import './style.css';
import logo from './logo.png';
import Commentaires from './commentaires';
import Searchbar from './searchbar';
import Stat from './stat';
import React, { Component } from 'react';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            caracterover: false,
            api: props.api,
            userid: props.userid,
            c: [true, true],
            search: '',
            contact: false
        }

        this.search = this.search.bind(this)
    }
    login() {
        this.props.login()
    }
    signin() {
        this.props.signin()
    }
    logout() {
        this.props.api.delete('/api/user/' + this.props.userid + '/logout')
            .then((response) => {
                console.log(response.data.message)
                this.props.logout()
            })
            .catch((error) => console.log(error))
    }
    profil(id) {
        this.props.profil(id)
    }
    send() {
        //const taille = 500;
        var m = document.getElementById('message')
        //this.setState({ caracterover: false })

        this.props.api.post('/apimessages/user/' + this.props.userid + '/messages', {
            message: m.innerText
        })
            .then((response) => {
                m.innerText = ''
                this.setState({ c: [true] })
            })
            .catch((error) => console.log(error))

        /*  
        if (taille >= m.innerText.trim().length & m.innerText.trim().length > 0) {//A changer par une requete HTTP
            this.setState({ commentaires: [{ user: 'moi', message: m.innerText }].concat(this.state.commentaires) })
            m.innerText = ''
        }
        if (m.innerText.trim().length > taille) {//A changer par une requete HTTP
            this.setState({ caracterover: true })
            var findutext = m.innerText.substring(taille)
            var debuttext = m.innerText.substring(0, taille)
            m.innerText = findutext
            var finTextHTML = '<mark>' + m.innerHTML + '</mark>'
            m.innerText = debuttext
            m.innerHTML += finTextHTML
        }*/
    }
    search(search = '', contact = false) {
        console.log("search : " + search + "\ncontact : " + contact)
        this.state.c[0] = true
        this.setState({ search: search, contact: contact })
    }
    render() {
        return (
            <div>
                <div>{this.state.test}</div>
                <header className='head'>
                    <div className='logo'>
                        <img src={logo} alt="Gazouillis" />
                    </div>
                    <Searchbar search={this.search} connecte={this.props.connecte}/>
                    <div>
                        {this.props.connecte ?
                            <div className='liencoreg'>
                                <button onClick={() => this.profil(this.props.userid)}>Profil</button>
                                <button onClick={() => this.logout()}>Deconnexion</button>
                            </div> :
                            <div className='liencoreg'>
                                <button onClick={() => this.login()}>Connexion</button>
                                <button onClick={() => this.signin()}>Enregistrement</button>
                            </div>}
                    </div>
                </header>
                <main className='main'>
                    <Stat userid={this.props.userid} page='main' c={this.state.c} api={this.props.api} />
                    <div className='decal' />
                    <div className='zcommentaire'>
                        <div className='lcommentaire'>
                            {this.props.connecte ?
                                <div className='newcomment'>
                                    <div>
                                        <p draggable='false' spellCheck='false' id="message" name="text" className="zcom" contentEditable />
                                        <div className='comHeader'>
                                            <button onClick={() => this.send()}>Envoyer</button>
                                            {this.state.caracterover ? <div className='warn'>trop de caractères</div> : <div />}
                                        </div>
                                    </div>
                                </div> : <div />}
                            <Commentaires search={this.state.search} contact={this.state.contact} login={this.props.login} c={this.state.c} connecte={this.props.connecte} profil={this.props.profil} page='main' api={this.props.api} uidPage={this.props.userid} userid={this.props.userid} />
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}

export default Main;