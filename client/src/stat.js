import './style.css';
import React, { Component } from 'react'

class Stat extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nombreUsers: -1,
            nombreAmies: -1,
            nombreMessages: -1,
            dateDernierMessage: 'JJ/MM/AAAA',
            v: 42
        }
    }
    componentDidMount() {
        if (this.props.page === 'main') {
            this.nbUsers()
            this.InfoMessagesTotal()
        } else {
            this.nbFriends()
            this.InfoMessages()
        }
    }
    date(d) {
        var date = new Date(d)
        var j = date.getDate()
        if (j < 10) {
            j = '0' + j
        }
        var m = date.getMonth() + 1
        if (m < 10) {
            m = '0' + m
        }
        var h = date.getHours()
        if (h < 10) {
            h = '0' + h
        }
        var min = date.getMinutes()
        if (min < 10) {
            min = '0' + min
        }
        return j + '/' + m + '/' + date.getFullYear() + ' ' + h + ':' + min
    }
    nbUsers() {
        this.props.api.get('/api/user/count')
            .then((response) => {
                console.log("Nombre d'utilisateurs : " + response.data.count)
                this.setState({ nombreUsers: response.data.count })
            })
            .catch((error) => console.log(error))
    }
    nbFriends() {
        this.props.api.get('/apifriends/user/' + this.props.oid + '/friends')
            .then((response) => {
                console.log("Nombre d'amies : " + response.data.length)
                this.setState({ nombreAmies: response.data.length })
            })
            .catch((error) => console.log(error))
    }
    InfoMessagesTotal() {
        this.props.api.get('/apimessages/infos')
            .then((response) => {
                console.log("Nombre de messages : " + response.data.count)
                this.setState({ nombreMessages: response.data.count })
                console.log("Date du dernier message : " + this.date(response.data.date))
                this.setState({dateDernierMessage: this.date(response.data.date)})
            })
            .catch((error) => console.log(error))
    }
    InfoMessages() {
        this.props.api.get('/apimessages/user/' + this.props.oid + '/infos')
            .then((response) => {
                console.log("Nombre de messages U: " + response.data.count)
                this.setState({ nombreMessages: response.data.count })
                console.log("Date du dernier message U: " + this.date(response.data.date))
                this.setState({ dateDernierMessage: this.date(response.data.date)})
            })
            .catch((error) => console.log(error))
    }
    delAccount() {
        if(this.state.v > 0) {
            this.setState({v: this.state.v - 7})
        }else {
            this.props.api.delete('/api/user/' + this.props.userid)
                .then((response) => {
                    console.log(response.data.message)
                    this.props.logout()
                })
                .catch((error) => console.log(error))
        }
    }
    componentDidUpdate() {
        if (this.props.c[1]) {
            if (this.props.page === 'main') {
                this.nbUsers()
                this.InfoMessagesTotal()
            } else {
                this.nbFriends()
                this.InfoMessages()
            }
            this.props.c[1] = false
        }
    }
    render() {
        if (this.props.page === 'main') {
            return (
                <div className='stat'>
                    <div className='statCase'>
                        <div className='statTitre'>Nombre d'utilisateurs</div>
                        <div className='statValeur'>{this.state.nombreUsers}</div>
                    </div>
                    <div className='statCase'>
                        <div className='statTitre'>Nombre de messages</div>
                        <div className='statValeur'>{this.state.nombreMessages}</div>
                    </div>
                    <div className='statCase'>
                        <div className='statTitre'>Date du dernier message</div>
                        <div className='statValeur'>{this.state.dateDernierMessage}</div>
                    </div>
                </div>)
        } else {
            return (
                <div className='stat'>
                    <div className='statCase'>
                        <div className='statTitre'>Nombre d'amies</div>
                        <div className='statValeur'>{this.state.nombreAmies}</div>
                    </div>
                    <div className='statCase'>
                        <div className='statTitre'>Nombre de messages</div>
                        <div className='statValeur'>{this.state.nombreMessages}</div>
                    </div>
                    <div className='statCase'>
                        <div className='statTitre'>Date du dernier message</div>
                        <div className='statValeur'>{this.state.dateDernierMessage}</div>
                    </div>
                    {this.props.userid === this.props.oid ?
                    <div className='delAccount-Case' onClick={() => this.delAccount()}>
                        <div className='delAccount-Titre'>Supprimer mon compte</div>
                        <div className='delAccount-count'>{this.state.v}</div>
                    </div>:
                    <div/>}
                </div>)
        }
    }
}

export default Stat;