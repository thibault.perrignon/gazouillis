import './style.css';
import React, { Component } from 'react'

class Commentaires extends Component {
    constructor(props) {
        super(props)
        this.state = { messages: [], friends: [] , users: []}
    }
    componentDidMount() {
        this.getF()
        this.getM()
        this.getU()
    }
    getF() {
        this.props.api.get('/apifriends/user/' + this.props.userid + '/friends')
            .then(res => {
                var ms = []
                for (var i = 0; i < res.data.length; i++) {
                    ms.push(res.data[i].idFriend)
                }
                this.setState({ friends: ms })
            })
            .catch(err => {
                console.log(err)
            })
    }
    getU() {
        this.props.api.get('/api/user/list')
            .then(res => {
                var ms = []
                for (var i = 0; i < res.data.list.length; i++) {
                    ms.push(res.data.list[i]._id)
                }
                this.setState({ users: ms })
            })
            .catch(err => {
                console.log(err)
            })
    }
    getM() {
        if (this.props.page === 'main' & this.props.contact === false) {

            this.props.api.get('/apimessages/messages')
                .then(res => {
                    var ms = []
                    for (var i = 0; i < res.data.length; i++) {
                        if (res.data[i].signal<10 && ( res.data[i].message.includes(this.props.search) || res.data[i].user.includes(this.props.search))) {
                            ms.push(res.data[i])
                        }
                    }
                    this.setState({ messages: ms })
                })
                .catch(err => {
                    console.log(err)
                })
        }
        if (this.props.page === 'profil' | this.props.contact === true) {
            this.props.api.get('/apimessages/user/' + this.props.uidPage + '/messages/friends')
                .then(res => {
                    var ms = []
                    for (var i = 0; i < res.data.length; i++) {
                        if (res.data[i].signal<10 && (res.data[i].message.includes(this.props.search) || res.data[i].user.includes(this.props.search))) {
                            ms.push(res.data[i])
                        }
                    }

                    this.setState({ messages: ms })
                })
                .catch(err => {
                    console.log(err)
                })
        }

    }
    addContact(u) {
        this.props.api.get('/api/user/' + this.props.userid)
            .then(res => {
                this.props.api.post('/apifriends/user/' + u + '/friends', { login: res.data.login })
                    .then(res => {
                        this.getF()
                    })
                    .catch(err => {
                        console.log(err)
                    })
            })
            .catch(err => {
                console.log(err)
            })
    }
    delContact(u) {
        this.props.api.delete('/apifriends/user/' + this.props.userid + '/friends/' + u)
            .then(res => {
                console.log(res.data)
                this.getF()
            })
            .catch(err => {
                console.log(err)
            })
    }
    profilOther(u) {
        this.props.profil(u)
        this.props.c[0] = true
        this.props.c[1] = true
    }
    date(d) {
        var date = new Date(d)
        var j = date.getDate()
        if (j < 10) {
            j = '0' + j
        }
        var m = date.getMonth() + 1
        if (m < 10) {
            m = '0' + m
        }
        var h = date.getHours()
        if (h < 10) {
            h = '0' + h
        }
        var min = date.getMinutes()
        if (min < 10) {
            min = '0' + min
        }
        return j + '/' + m + '/' + date.getFullYear() + ' ' + h + ':' + min
    }
    edit(id) {
        if (document.getElementById(id).contentEditable === 'true') {
            document.getElementById(id).contentEditable = 'false';
            document.getElementById(id).className = 'pa'
            this.props.api.put('/apimessages/user/' + this.props.userid + '/messages', { message: document.getElementById(id).innerText, message_id: id })
                .then(res => {
                    console.log(res.data)
                    this.getM()
                })
                .catch(err => {
                    console.log(err)
                })
        }
        else {
            document.getElementById(id).contentEditable = 'true'
            document.getElementById(id).className = 'zcom'
        }
    }
    del(id) {
        console.log(id)
        this.props.api.delete('/apimessages/user/' + this.props.userid + '/messages/' + id)
            .then(res => {
                console.log(res.data)
                this.getM()
            })
            .catch(err => {
                console.log(err)
            })
            
    }
    signal(id,uid) {
        console.log("signalement : " + id)
        this.props.api.post('/apimessages/messages/' + id + '/signal',{user_id:uid})
            .then(res => {
                console.log(res.data)
            })
            .catch(err => {
                console.log(err)
            })
    }
    com(c) {
        var comm = c.sort((a, b) => b.date - a.date).map((n) =>
            <div className='commentaire'>
                <div className='comHeader'>
                    <div>
                        {this.state.users.includes(n.user_id)?
                        <button onClick={() => this.profilOther(n.user_id)}>
                            {n.user}
                        </button>:
                        <button disabled>
                            {n.user}
                        </button>}
                        {this.props.connecte & n.user_id !== this.props.userid & !(!this.state.friends.includes(n.user_id) & !this.state.users.includes(n.user_id))?
                            (!this.state.friends.includes(n.user_id) & this.state.users.includes(n.user_id)? <button onClick={() => this.addContact(n.user_id)}>+</button> :
                                <button onClick={() => this.delContact(n.user_id)}>-</button>) :
                            <button disabled>+</button>}
                        {this.date(n.date)}
                    </div>
                    {n.user_id === this.props.userid ? <div><button onClick={() => this.edit(n._id)}>Edit</button><button onClick={() => this.del(n._id)}>Delete</button></div> : <div />}
                </div>
                <p className='pa' id={n._id} />
                {n.date !== n.lastEdit ? <div className='bd'>Last edit: {this.date(n.lastEdit)}</div> : <div />}
                <div className='sig'><button onClick={() => this.signal(n._id,n.user_id)}>Signal</button></div>
            </div>
        );

        return comm
    }
    componentDidUpdate() {
        if (this.props.c[0]) {
            this.getM()
            this.props.c[0] = false
        }

        var c = this.state.messages

        for (var i = 0; i < c.length; i++) {
            let m = c[i]
            if (document.getElementById(m._id)) {
                document.getElementById(m._id).innerText = m.message
            }

            
        }
    }
    render() {
        return (
            <div>
                {this.com(this.state.messages)}
            </div>
        )
    }
}

export default Commentaires;