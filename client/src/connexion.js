import './style.css';
import React, { Component } from 'react'

class Connexion extends Component {
    constructor(props) {
        super(props)
        this.state = {warn: ''}
    }
    cancel() {
        this.props.cancel()
    }
    connexion() {
        var login = document.getElementById("login").value
        var pw = document.getElementById("passw").value
        
        this.props.api.post('/api/user/login', { "login": login, "password": pw })
            .then(res => {
                if (res.status === 200) {
                    console.log("Connexion réussi")
                    this.props.connect(res.data.userid)
                }
            })
            .catch(err => {
                if (err.response.status === 400) {
                    this.setState({ warn: "Login et mot de passe nécessaires" })
                    console.log("login et password nécessaires")
                }
                if (err.response.status === 401) {
                    this.setState({warn : "Utilisateur non reconnu"})
                    console.log("Utilisateur inconnu")
                }
                if (err.response.status === 403) {
                    this.setState({warn : "Mot de passe incorrect"})
                    console.log("Mot de passe invalide")
                }
                if (err.response.status === 500) {
                    console.log("Erreur interne")
                }
            }
        )
    }
    render() {
        return (
            <div>
                <p className='titre'>Ouvrir une session</p>
                <div className='center'>
                    <form className='boitecoreg' onSubmit={(e) => e.preventDefault()}>
                        <div className='just'>
                            Login
                            <input type="text" id="login" name="name" required maxLength="32" size="10" className='space' />
                        </div>
                        <div className='just'>
                            Mot de passe
                            <input type="password" id="passw" name="pass" required maxLength="32" size="10" className='space' />
                        </div>
                        <div className='warn2'>
                            {this.state.warn}
                        </div>
                        <div className='just'>
                            <button onClick={() => this.connexion()}>Connexion</button>
                            <button onClick={() => this.cancel()}>Annuler</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Connexion;