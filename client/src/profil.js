import './style.css';
import logo from './logo.png';
import Commentaires from './commentaires';
import Stat from './stat';
import React, { Component } from 'react';

class Profil extends Component {
    constructor(props) {
        super(props);
        this.state = {
            caracterover: false,
            pseudo: '',
            c: [true,true]
        }
    }
    componentDidMount() {
        this.userlogin()
    }
    userlogin() {
        this.props.api.get('/api/user/' + this.props.oid)
            .then((response) => {
                this.setState({ pseudo: response.data.login })
            })
            .catch((error) => console.log(error))
    }
    logout() {
        this.props.api.delete('/api/user/' + this.props.userid + '/logout')
            .then((response) => {
                console.log(response.data.message)
                this.props.logout()
            })
            .catch((error) => console.log(error))
    }
    login() {
        this.props.login()
    }
    signin() {
        this.props.signin()
    }
    accueil() {
        this.props.accueil()
    }
    render() {
        if(this.state.c[0]){
            this.userlogin()
            this.setState({c:[false]})
        }
        return (
            <div>
                <div>{this.state.test}</div>
                <header className='head'>
                    <div className='logo'>
                        <img src={logo} alt="Gazouillis" />
                    </div>
                    <div className='titre'>
                        Page de profil de {this.state.pseudo}
                    </div>
                    <div className='liencoreg'>
                        <button onClick={() => this.accueil()}>Accueil</button>
                        {this.props.connecte ?
                            <button onClick={() => this.logout()}>Deconnexion</button> :
                            <div className='sliencoreg'>
                                <button onClick={() => this.login()}>Connexion</button>
                                <button onClick={() => this.signin()}>Enregistrement</button>
                            </div>}
                    </div>
                </header>
                <main className='main'>
                    <Stat logout={this.props.logout} oid={this.props.oid} page='profil' c={this.state.c} api={this.props.api} userid={this.props.userid}/>
                    <div className='decal' />
                    <div className='zcommentaire'>
                        <div className='lcommentaire'>
                            <Commentaires search='' contact={true} login={this.props.login} c={this.state.c} connecte={this.props.connecte} profil={this.props.profil} page='profil' api={this.props.api} uidPage={this.props.oid} userid={this.props.userid} />
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}

export default Profil;